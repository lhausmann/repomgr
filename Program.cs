﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace repomgr
{
    public static class Program
    {
        public static RepoMgr Repo;

        public static void Main(string[] args)
        {
            if (File.Exists("/tmp/repomgr.lock") && args[1] != "daemon")
            {
                Console.WriteLine("/tmp/repomgr.lock exists, delete it if you are sure no other process is running");
                Environment.Exit(1);
            }
            else if (args[1] != "daemon")
            {
                File.Create("/tmp/repomgr.lock");
            }

            if (args.Length < 2 || args[1].Equals("help"))
                PrintHelp();
            Repo = new RepoMgr(args[0]);
            if (File.Exists(Path.Combine(args[0], "repomgr.index.json")))
                Repo.ReadIndex();
            switch (args[1])
            {
                case "daemon":
                    CreateWebHostBuilder(args).Build().Run();
                    break;
                case "init":
                    if (args.Length != 4)
                        PrintHelp();
                    try
                    {
                        Repo.Init(args[2], args[3]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Init failed with error: " + e);
                    }

                    break;
                case "add":
                    if (args.Length != 3)
                        PrintHelp();
                    try
                    {
                        Repo.Add(args[2]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Add failed with error: " + e);
                    }

                    break;
                case "update":
                    if (args.Length < 3 || args.Length > 4)
                        PrintHelp();
                    try
                    {
                        switch (args.Length)
                        {
                            case 3:
                                Repo.Build(args[2]);
                                break;
                            case 4 when args[3] == "-f":
                                Repo.Build(args[2], true);
                                break;
                            default:
                                PrintHelp();
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Build failed with error: " + e);
                    }

                    break;
                case "update-all":
                    if (args.Length != 2)
                        PrintHelp();
                    try
                    {
                        Repo.BuildAll();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("BuildAll failed with error: " + e);
                    }

                    break;
                case "remove":
                    if (args.Length != 3)
                        PrintHelp();
                    try
                    {
                        Repo.Remove(args[2]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Remove failed with error: " + e);
                    }

                    break;
                case "list":
                    if (args.Length != 2)
                        PrintHelp();
                    try
                    {
                        Repo.List();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("List failed with error " + e);
                    }

                    break;
                default:
                    PrintHelp();
                    break;
            }
            if (File.Exists("/tmp/repomgr.lock"))
                File.Delete("/tmp/repomgr.lock");
        }

        private static void PrintHelp()
        {
            //TODO: add/remove <package> [...]
            Console.WriteLine("Usage:");
            Console.WriteLine("repomgr <data-path> init <repo-path> <reponame>");
            Console.WriteLine("repomgr <data-path> list");
            Console.WriteLine("repomgr <data-path> add <package>");
            Console.WriteLine("repomgr <data-path> remove <package>");
            Console.WriteLine("repomgr <data-path> update <package> [-f]");
            Console.WriteLine("repomgr <data-path> update-all");
            Console.WriteLine("repomgr <data-path> daemon");
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}