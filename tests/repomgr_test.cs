using System;
using System.IO;
using System.Linq;
using repomgr;
using Xunit;

namespace tests
{
    public class RepomgrTest
    {
        private static RepoMgr Init(string folder)
        {
            var repo = new RepoMgr(folder);
            repo.Init(Path.Combine(folder, "repo"), "xunit");
            return repo;
        }

        private static void Add(RepoMgr repo, string package)
        {
            repo.ReadIndex();
            repo.Add(package);
            Assert.Contains(repo._repo.Packages, p => p.Name == package);
            Assert.Contains(package, File.ReadAllText(Path.Combine(repo._buildpath, "repomgr.index.json")));
        }

        private static void Build(RepoMgr repo, string package)
        {
            repo.ReadIndex();
            repo.Build(package);
        }
        
        private static void Remove(RepoMgr repo, string package)
        {
            repo.ReadIndex();
            repo.Remove(package);
        }

        private static string GetTestFolder()
        {
            return "xunit-" + Guid.NewGuid();
        }

        [Fact]
        public void TestInit()
        {
            var folder = GetTestFolder();
            Init(folder);
            Assert.True(Directory.Exists(Path.Combine(folder, "repo")));
            Assert.True(File.Exists(Path.Combine(folder, "repomgr.index.json")));
            Directory.Delete(folder, true);
        }

        [Theory]
        [InlineData("adduser")]
        [InlineData("yay")]
        [InlineData("goimports-git")]
        public void TestAdd(string package)
        {
            var folder = GetTestFolder();
            var repo = Init(folder);
            Add(repo, package);
            Assert.True(File.Exists(Path.Combine(folder, "pkg", package, "PKGBUILD")));
            var pkg = repo._repo.Packages.FirstOrDefault(p => p.Name.Equals(package));
            Assert.NotNull(pkg);
            Assert.Equal("never-updated", pkg.CurrentVersion);
            Assert.Equal("nA", pkg.RepoVersion);
            Assert.Empty(pkg.PkgFiles);
            Assert.True(pkg.LastBuildSucceeded);
            Directory.Delete(folder, true);
        }

        [Theory]
        [InlineData("adduser")]
        [InlineData("yay")]
        [InlineData("goimports-git")]
        public void TestBuild(string package)
        {
            var folder = GetTestFolder();
            var repo = Init(folder);
            Add(repo, package);
            Build(repo, package);
            Assert.True(Directory.Exists(Path.Combine(folder, "pkg", package)));
            Assert.True(File.Exists(Path.Combine(folder, "repo", "xunit.db.tar.gz")));
            Directory.Delete(folder, true);
        }
        
        [Theory]
        [InlineData("adduser")]
        [InlineData("yay")]
        [InlineData("goimports-git")]
        public void TestRemove(string package)
        {
            var folder = GetTestFolder();
            var repo = Init(folder);
            Add(repo, package);
            Build(repo, package);
            Remove(repo, package);
            Assert.False(File.Exists(Path.Combine(folder, "pkg", package, "PKGBUILD")));
            var pkg = repo._repo.Packages.FirstOrDefault(p => p.Name.Equals(package));
            Assert.Null(pkg);
            Directory.Delete(folder, true);
        }
    }
}