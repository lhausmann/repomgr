## repomgr

This software allows you to automatically manage any archlinux repo with git repos as source.

### Usage:

First, initialize your repository using `repomgr /path/to/data init /path/to/repo repo`

Next, add a package. `repomgr /path/to/data add <package>`

Then, build the package and update the repo. `repomgr /path/to/data update <package>`

Now you can list your packages using `repomgr /path/to/data list`

If you want to automatically update your packages, set up a cronjob for `repomgr /path/to/data update-all`.

You can also remove packages using `repomgr /path/to/data remove <package>`

In case you want a webinterface to view the status of the builds, check out `repomgr /path/to/data daemon`.
You can change the port with the environment variable `ASPNETCORE_URLS`. For example, to change it to port 5777,
use `ASPNETCORE_URLS='http://*:5777' repomgr /path/to/data daemon`